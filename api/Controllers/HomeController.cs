using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using common;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers
{
    [Route("api/Home/[action]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        public ActionResult<IEnumerable<string>> Get()
        {
            NLogHelper.Info("这是info日志");
            NLogHelper.Error("这是error日志",new Exception("测试"));

            //这里随便返回一下
            return new string[] { "value1", "value2" };
        }
    }
    
}