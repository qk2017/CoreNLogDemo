using System;
using Microsoft.Extensions.Logging;
using NLog;

namespace common
{
    /**
        NLog日志帮助类
        这里没有依赖注入到构造方法中，而是封装了日志公共类，但是日志输出的方法中，所有异常都来自NLogHelper类，不便于定位日志位置，
        所以配置文件中假如${callsite:className=true:methodName=true:skipFrames=1}即可
     */
    public class NLogHelper
    {
        //每创建一个Logger都会有一定的性能损耗，所以定义静态变量
        private static Logger nLogger = LogManager.GetCurrentClassLogger();
        
        public static void Info(string msg)
        {
            nLogger.Info(msg);
        }        
        
        public static void Error(string msg, Exception ex = null)
        {
            if (ex == null)
                nLogger.Error(msg);
            else
                nLogger.Error(ex,msg);
        }
    }
}
